from guardian import GuardState, GuardStateDecorator, NodeManager, Node
import cdsutils
import gpstime


import time, os, sys, datetime
import numpy as np
import matplotlib.pyplot as plt


# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

from optic import Optic


SUB_GRD_STATES = ['SUS', 'PMC','TTFSS', 'SHG', 'OPO', 'CLF', 'ISS',  'FC', 'LO']

NOM_STATES = {'SQZ_TTFSS':'LOCKED', 'SQZ_PMC':'LOCKED', 'SQZ_SHG':'LOCKED', 'SQZ_OPO':'LOCKED', 'SQZ_CLF':'CLF_REFL_LOCKED', 'SQZ_LO':'LOCKED','SQZ_FC':'LOCKED','SQZ_SUS':'SERVO_PSAMS','SQZ_ISS':'LOCKED'}

LOCKED_STATES = {'SQZ_TTFSS':['LOCKED','LOCKING'],
		 'SQZ_PMC':['LOCKED','LOCKING','LOCKED_and_SLOW'],
                 'SQZ_SHG':['LOCKED','LOCKING','LOCKED_and_SLOW'],
                 'SQZ_OPO':['LOCKED_WITH_CLF','CLF_DITHER_LOCKED','LOCKED'],
                 'SQZ_CLF':['LOCKING','LOCKED','LOCKED_and_SLOW','CLF_REFL_LOCKED','SWITCH_CLFR_TO_OMCQ','OMCQ_LOCKED','SWITCH_OMCQ_TO_CLFR'],
                 'SQZ_FC':['LOCKED'],
                 'SQZ_ISS':['LOCKED','CLF_ISS_LOCKED',],
                 'SQZ_SUS':['RECORD_ASC_OUTPUT','PSAMS_ENGAGED'],
                 'SQZ_LO':['LOCKED'],
}


    
SUB_GRDS = ['SQZ_%s' % s for s in SUB_GRD_STATES]

#REMOVING SQZ_TRANSITIONS:
nodes = NodeManager(SUB_GRDS)

if sqzconfig.nosqz:
    #nominal = 'NO_SQUEEZING'
    nominal = 'LOCK_TTFSS'
else:
    nominal = 'FREQ_DEPENDENT_SQZ'

#So that editors don't complain about missing variables
#requests = {}

def notify_log(msg):
    notify(msg)
    log(msg)

##################################
############ CHECKER #############
##################################

def are_dependencies_bad(REQs):
    for req in REQs:
        node = nodes[req]
        #REMOVING SQZ_TRANSITIONS:#
        if node.managed and node.check_fault() and node.request != 'MANAGED':
            notify('%s guardian failed'%node.name)
            nodes.release()
            return 'WAITING_FOR_MANAGED'

    # check for sub-guardians that have consistently
    # failed to re-acquire lock -> close beam diverter
    for node in nodes:
        if node.managed and node.state == 'REQUIRES_INTERVENTION':
            return 'NEEDS_USER_INTERVENTION'

    for grd in SUB_GRDS:
        # if guardian is not in the requested nominal state
        # (or a state from which it can reach its nominal state)
        if check_unlocked(nodes[grd]):
            if grd in REQs:
                notify_log("RESETTING from subgrd {}".format(grd))
                return "RESET"
    return


##################################
####### HELPER FUNCTIONS #########
##################################

def check_unlocked(node):
    return not node.state in LOCKED_STATES[node.name]

def set_state(nodeName, newState):
    nodes[nodeName] = newState
    #requests[nodeName] = newState

def homodyne_check():
    if ezca['SYS-MOTION_C_BDIV_E_POSITION']== 0:
        log('Close beam diverter')
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1
        return True


    #if ezca['SQZ-OPO_IRSELECT_FIBERSWITCH_READBACK']:
        #log('Switch flipper to CLF')
        #ezca['SQZ-OPO_IRSELECT_FIBERSWITCH_CONTROL'] = 0
        #return True
    if ezca['SQZ-CLF_FLIPPER_CONTROL'] == 0:
        log('Open CLF flipper')
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0
        return True

    return True

def ifo_check():
    if ezca['GRD-ISC_LOCK_STATE_N'] < 1100:
        notify_log('Waiting for IFO to reach ENGAGE_SQZ')
        return

    return True

##################################
####### ALIGNMENT STUFF #########
##################################

fc2 = Optic('FC2')

##################################
############# STATES #############
##################################

class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        nodes.set_managed()
        #also, set the local request state to the current request state, so that
        #we aren't stuck thinking that the user has changed a request state
        #this is weird, looking, but it relies on the difference between the get and set
        #actions, set additionally records the request set of THIS manager,
        #to compare in check_fault
        for node in nodes:
            node.set_request(node.request)


class IDLE(GuardState):
    index = 1
    goto = True
    request = True

    def main(self):
        nodes.release()
        return True


#-------------------------------------------------------------------------------
#
class WAITING_FOR_MANAGED(GuardState):
    index = 3
    request = False
    redirect = False

    def run(self):
        for node in nodes:
            if node.request == 'MANAGED':
                nodes.set_managed()
                #also, set the local request state to the current request state, so that
                #we aren't stuck thinking that the user has changed a request state
                #this is weird, looking, but it relies on the difference between the get and set
                #actions, set additionally records the request set of THIS manager,
                #to compare in check_fault
                for node in nodes:
                    node.set_request(node.request)
                return True
        notify_log('Revive manager by requesting INIT state')
        return False

#-------------------------------------------------------------------------------
class DOWN(GuardState):
    index = 5
    request = False
    goto = False

    def main(self):
        self.counter = 0
        self.timer['done'] = 0

        # Set some flags
        self.set_sub_grds_to_idle = True
        self.wait_for_sub_grds = False
        self.close_bdiv = False
        self.wait_for_bdiv = False
        
    def run(self):

        if not self.timer['done']:
            return

        if self.set_sub_grds_to_idle:
            for node in nodes:
                node.set_request('IDLE')
            self.timer['done'] = 1
            self.set_sub_grds_to_idle = False
            self.wait_for_sub_grds = True
            return

        if self.wait_for_sub_grds:
            if not all([n.arrived for n in nodes]):
                return
            self.wait_for_sub_grds = False
            self.close_bdiv = True
            return

        if self.close_bdiv:
            ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1
            self.timer['done'] = 1
            self.close_bdiv = False
            self.wait_for_bdiv = True
            return

        if self.wait_for_bdiv:
            if ezca['SYS-MOTION_C_BDIV_E_BUSY']:
                return
            self.wait_for_bdiv = False

        return True

#-------------------------------------------------------------------------------
#
class NO_SQUEEZING(GuardState):
    index = 7
    goto = False
    request = True

    def main(self):

        ezca["SYS-MOTION_C_BDIV_E_CLOSE"] = 1

        time.sleep(5)
        return

    def run(self):
        if not  all([n.arrived for n in nodes]):
            return 'DOWN'
        return all([n.arrived for n in nodes])


#-------------------------------------------------------------------------------
#
class NEEDS_USER_INTERVENTION(GuardState):
    index = 10
    request = True
    redirect = True

    def main(self):
        log("HELP")
        pass
        ######################################################
        ###### TODO: remove this when we're ready to close beam diverter

    def run(self):
        needsAttention = [n.name for n in nodes if (n.state == 'REQUIRES_INTERVENTION' and n.managed)]
        if len(needsAttention) == 0:
            return True
        else:
            notify_log('Need user attention on %s' % (', '.join(needsAttention)))

#-------------------------------------------------------------------------------
#
class RESET(GuardState):
    index = 20
    goto = False
    redirect = True
    request = False

    def main(self):
        self.timer['done'] = 0
        self.counter = 0

    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            # idle FC and suspension
            nodes['SQZ_FC'].set_request('IDLE')
            nodes['SQZ_SUS'].set_request('IDLE')
            nodes['SQZ_CLF'].set_request('IDLE')
            nodes['SQZ_OPO'].set_request('IDLE')
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 1:
            if nodes['SQZ_FC'].arrived:
                # reset SQZ calibration parameter
                ezca['SQZ-DCPD_NULL_LP_3_GAIN'] = 1
                # Close beam diverter and turn off ASC
                ezca['SYS-MOTION_C_BDIV_E_CLOSE']=1

                # set guardians to idle after the
                # first downed guardian
                down = False
                for j in range(1, len(SUB_GRDS)):
                    if check_unlocked(nodes[SUB_GRDS[j]]):
                        down = True
                    else:
                        nodes[SUB_GRDS[j]] = NOM_STATES[SUB_GRDS[j]]

                    if down:
                        set_state(SUB_GRDS[j], 'IDLE')

                self.counter += 1        

        elif self.counter == 2:
            if nodes['SQZ_OPO'].arrived:
                #close green to OPO flipper mirror
                ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 0
                self.counter += 1        

        elif self.counter == 3:
            # wait for beam diverter to open
            if not ezca['SYS-MOTION_C_BDIV_E_BUSY']:        
                return True

#-------------------------------------------------------------------------------
class SERVO_PSAMS(GuardState):
    index = 25
    request = False
    DEPS = frozenset([])

    def main(self):
        self.SUS_node = nodes['SQZ_SUS']
        self.SUS_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.SUS_node.set_managed()
        
        self.timer['stalled'] = 2
        self.timer['done'] = 0        
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        # Set SUS node to Offloads PSAMs if output is larger than threshold
        if self.counter == 0:
            for sus in ['ZM2','ZM4','ZM5']:
                if abs(ezca['AWC-%s_M2_SAMS_SERVO_OUTPUT'%sus]) > 5:
                    self.SUS_node.set_request('OFFLOAD_PSAMS_SERVO_OUTPUT')
            self.timer['stalled'] = 2
            self.counter += 1

        # Waits for the SUS node to arrive (finish offloading), and/or revives the node if it stalls
        elif self.counter == 1:
            if self.SUS_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.SUS_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: SUS GRD fail %d times.'% self.failcounter)

            else:
                notify('waiting for PSAMS servo offloading')
                self.timer['stalled'] = 2                
                if self.SUS_node.completed:
                    self.counter += 1

        # Sets the SUS node to engage PSAMs
        elif self.counter == 2:
            self.SUS_node.set_request('PSAMS_ENGAGED')
            self.timer['stalled'] = 2
            self.counter += 1                                       

        # Wait for SUS node to arrive, revive if stalled (notify after 4 failed attempts)
        elif self.counter == 3:            
            if self.SUS_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.SUS_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: SUS GRD fail %d times.'% self.failcounter)

            else:
                notify('waiting for PSAMS')
                self.timer['stalled'] = 2                
                if self.SUS_node.completed:
                    self.counter += 1
                    

        elif self.counter == 4:
            return True
        
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class LOCK_TTFSS(GuardState):
    index = 27
    request = False
    DEPS = frozenset(['SQZ_SUS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.TTFSS_node = nodes['SQZ_TTFSS']
        self.TTFSS_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.TTFSS_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
            bdep_jump = are_dependencies_bad(self.DEPS)
            if bdep_jump: return bdep_jump

        # Check if TTFSS node already LOCKED
        
        #if self.counter == 0 and self.TTFSS_node.state == LOCKED_STATES['SQZ_TTFSS']:
            #notify('TTFSS already locked.')
            #return True
        
            # Set TTFSS node to LOCK
        #else:
            if self.counter == 0:
                self.TTFSS_node.set_request('LOCKED')
                self.counter += 1

            # Wait for TTFSS node to arrive, revive if it stalls
            elif self.counter == 1:
                if self.TTFSS_node.STALLED:
                    if self.timer['stalled']:
                        self.failcounter += 1
                        self.TTFSS_node.revive()
                        if self.failcounter > 4:
                            notify('Warning: TTFSS loop fail to be locked %d times.'% self.failcounter)

                else:
                    notify('waiting for TTFSS locked')
                    self.timer['stalled'] = 2                
                    if self.TTFSS_node.completed:
                        self.counter += 1
                

            elif self.counter == 2:
                return True

#-------------------------------------------------------------------------------

class LOCK_PMC(GuardState):
    index = 28
    request = False
    DEPS = frozenset(['SQZ_TTFSS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.PMC_node = nodes['SQZ_PMC']
        self.PMC_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO ??
        self.PMC_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set PMC node to lock the PMC
        if self.counter == 0:
            self.PMC_node.set_request('LOCKED')
            self.counter += 1

        # Wait for PMC node to arrive, if node stalls, revive
        elif self.counter == 1:
            if self.PMC_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.PMC_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: PMC loop failed to be locked %d times.'% self.failcounter)

            else:
                notify('waiting for PMC locked')
                self.timer['stalled'] = 2                
                if self.PMC_node.arrived:
                    self.counter += 1
                

        elif self.counter == 2:
            return True


#-------------------------------------------------------------------------------
#
class LOCK_SHG(GuardState):
    index = 30
    request = False
    DEPS = frozenset(['SQZ_PMC'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.SHG_node = nodes['SQZ_SHG']
        self.SHG_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.SHG_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set SHG node to lock the SHG, and then optimize by tuning temperature
        if self.counter == 0:
            self.SHG_node.set_request('LOCKED')
            self.counter += 1

        # Wait for SHG node to arrive, if node stalls, revive
        elif self.counter == 1:
            if self.SHG_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.SHG_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: SHG loop fail to be locked %d times.'% self.failcounter)

            else:
                notify('waiting for SHG locked')
                self.timer['stalled'] = 2                
                if self.SHG_node.arrived:
                    self.counter += 1
                

        elif self.counter == 2:
            return True
            
#-------------------------------------------------------------------------------
class ADJUST_GR_POL(GuardState):
    index = 31
    request = False
    DEPS = frozenset(['SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        self.counter = 0
        self.step_size = 0
        #self.HWP_pos = ezca['SYS-MOTION_C_PICO_I_MOTOR_3_X_POSITION']
        self.rej_pwr = ezca['SQZ-SHG_FIBR_REJECTED_DC_POWER']
        self.rej_pwr0 = self.rej_pwr
        self.direction = 1
        self.subcounter = 0
        self.state = False
        self.bigcounter = 0
        self.thresh = 0.01
        self.pos = 0
        
    def sweep_WP(self, channel1, channel2):

        
        if self.counter == 0:
            ezca['SYS-MOTION_C_PICO_I_CURRENT_STEPSIZE'] = 100 #medium
            self.step_size = 100
            self.pos = ezca[channel1]
            ezca[channel2] = self.pos + self.step_size * self.direction
            ezca['SYS-MOTION_C_PICO_I_CURRENT_GO'] = 1
            time.sleep (2)
            self.rej_pwr = ezca['SQZ-SHG_FIBR_REJECTED_DC_POWER']
            
            # find the initial direction
            if self.subcounter == 0:
                if self.rej_pwr > self.rej_pwr0:
                    self.direction = -1 * self.direction
                    notify('Wrong direction, swapped it.')
                    
                else: 
                    self.rej_pwr0 = self.rej_pwr
                    self.subcounter += 1
                    notify('Right direction, keep going.')
                    
            # go down until you find the minima: step and check pwr every step
            if self.subcounter == 1:
                self.pos = ezca[channel1]
                ezca[channel2] = self.pos + self.step_size * self.direction
                #ezca[channel] += self.step_size * self.direction
                ezca['SYS-MOTION_C_PICO_I_CURRENT_GO'] = 1
                time.sleep (2)
                self.rej_pwr = ezca['SQZ-SHG_FIBR_REJECTED_DC_POWER']
                
                if self.rej_pwr < self.rej_pwr0:
                    self.rej_pwr0 = self.rej_pwr
                    notify('Keep going.')
                else: 
                    self.pos = ezca[channel1]
                    ezca[channel2] = self.pos - self.step_size * self.direction
                    ezca['SYS-MOTION_C_PICO_I_CURRENT_GO'] = 1
                    time.sleep (2)
                    self.counter += 1
                    self.subcounter += 1
                    notify('Hit minima, go back.')
                    
                    
        elif self.counter == 1:
            return True
    
        
    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        if ezca['SYS-MOTION_C_PICO_I_CURRENT_ENABLE'] == 0:
            ezca['SYS-MOTION_C_PICO_I_CURRENT_ENABLE'] = 1
            time.sleep(60)
            
        
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 1
        time.sleep(2)
        
        
        if self.rej_pwr <= self.thresh:
            notify('Already below threshold.')
            return True 

        # Sweep HWP first to minimize rejected light
        if (self.bigcounter % 2) == 0 and self.bigcounter <= 10:
            ezca['SYS-MOTION_C_PICO_I_SELECTEDMOTOR'] = 3
            channel1 = 'SYS-MOTION_C_PICO_I_MOTOR_3_X_POSITION'  
            channel2 = 'SYS-MOTION_C_PICO_I_CURRENT_X_MOVETO'
            self.counter = 0
            self.subcounter = 0
            self.state = self.sweep_WP(channel1, channel2)
            notify('Sweeping HWP')
            
            if self.state == True: 
                self.bigcounter += 1
                
        if self.rej_pwr <= self.thresh:
            notify('Below threshold.')
            return True 
                
        # Sweep QWP to minimize rejected light
        if (self.bigcounter % 2) == 1:
            channel1 = 'SYS-MOTION_C_PICO_I_MOTOR_3_Y_POSITION'
            channel2 = 'SYS-MOTION_C_PICO_I_CURRENT_Y_MOVETO'
            self.counter = 0
            self.subcounter = 0  
            self.state = self.sweep_WP(channel1, channel2)
            notify('Sweeping QWP')
            
            if self.state == True: 
                self.bigcounter += 1      
                
        if self.rej_pwr <= self.thresh:
            notify('Below threshold.')
            return True 
            
        if self.bigcounter >= 10: 
            notify('Way too many tryouts! Get a human for help.')
            return False
                
            
#-------------------------------------------------------------------------------

class WAIT_IFO_1(GuardState):
    index = 50
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])

    def main(self):
        None

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        return True


#-------------------------------------------------------------------------------
class LOCK_SINGLE_CLF(GuardState):
    index = 55
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.OPO_node = nodes['SQZ_OPO']

        self.OPO_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.OPO_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.counter == 0:
            self.OPO_node.set_request('CLF_DITHER_LOCKED')
            self.counter += 1

        elif self.counter == 1:
            if self.OPO_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.OPO_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: OPO loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for OPO locked')
                self.timer['stalled'] = 2                
                if self.OPO_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class MISALIGN_FC2_1ST(GuardState):
    index = 60
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        #ezca['SUS-FC2_M1_TEST_Y_TRAMP'] = 3
        #ezca['SUS-FC2_M1_TEST_Y_OFFSET'] = 1000

        fc2.misalign('Y',1000,ezca)

        self.timer['done'] = 5 #0
        #self.counter = 0

    def run(self):

        if not self.timer['done']:
            return

        #if self.counter == 0:
        #    ezca.switch('SUS-FC2_M1_TEST_Y','OFFSET','ON')
        #    self.counter += 1
        #    self.timer['done'] = 3

        #elif self.counter == 1:

        return True

#-------------------------------------------------------------------------------
class OPEN_BDIV_1ST(GuardState):
    index = 65
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO'])
    
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        if not self.timer['done']:
            return

        if ezca['SQZ-SEED_FLIPPER_CONTROL']:
            notify('close the seed shutter')
            return

        if self.counter == 0:
            ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1
            self.timer['timeout'] = 10
                       
            self.counter += 1

        elif self.counter == 1:
            if not ezca['SYS-MOTION_C_BDIV_E_POSITION'] and not ezca['SYS-MOTION_C_BDIV_E_BUSY']:
                self.counter += 1
            if self.timer['timeout']:
                notify('Can\'t open beam diverter.')
                return

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class SINGLE_CLF_ADS(GuardState):
    index = 70
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG','SQZ_OPO'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.CLF_node = nodes['SQZ_CLF']

        self.CLF_node.revive()
        self.CLF_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.counter == 0:
            self.CLF_node.set_request('SINGLE_CLF_ALIGNED')
            self.counter += 1

        elif self.counter == 1:
            if self.CLF_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.CLF_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: CLF loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for CLF locked')
                self.timer['stalled'] = 2                
                if self.CLF_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class SINGLE_ADF_ADS(GuardState):
    index = 75
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG','SQZ_OPO'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.CLF_node = nodes['SQZ_CLF']

        self.CLF_node.revive()
        self.CLF_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.counter == 0:
            self.CLF_node.set_request('SINGLE_ADF_ALIGNED')
            self.counter += 1

        elif self.counter == 1:
            if self.CLF_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.CLF_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: CLF loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for CLF locked')
                self.timer['stalled'] = 2                
                if self.CLF_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class RESET_OPO(GuardState):
    index = 110
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.CLF_node = nodes['SQZ_CLF']
        self.OPO_node = nodes['SQZ_OPO']        

        self.CLF_node.revive()
        self.OPO_node.revive()
        self.OPO_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.counter == 0:
            self.CLF_node.set_request('IDLE')
            self.counter += 1

        elif self.counter == 1:
            if self.CLF_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.CLF_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: CLF loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for CLF')
                self.timer['stalled'] = 2                
                if self.CLF_node.completed:
                    self.counter += 1

        if self.counter == 2:
            self.OPO_node.set_request('IDLE')
            self.counter += 1

        elif self.counter == 3:
            if self.OPO_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.OPO_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: OPO loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for CLF')
                self.timer['stalled'] = 2                
                if self.OPO_node.completed:
                    self.counter += 1

        elif self.counter == 4:
            return True        
    
#-------------------------------------------------------------------------------
class CLOSE_BDIV(GuardState):
    index = 120
    request = False
    
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

    def run(self):
        if not self.timer['done']:
            return

        if ezca['SQZ-SEED_FLIPPER_CONTROL']:
            notify('close the seed shutter')
            return

        if self.counter == 0:
            ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1
            self.timer['timeout'] = 10
                       
            self.counter += 1

        elif self.counter == 1:
            if ezca['SYS-MOTION_C_BDIV_E_POSITION'] and not ezca['SYS-MOTION_C_BDIV_E_BUSY']:
                self.counter += 1
            if self.timer['timeout']:
                notify('Can\'t close beam diverter.')
                return

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class REALIGN_FC2(GuardState):
    index = 128
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        #ezca['SUS-FC2_M1_TEST_Y_TRAMP'] = 3

        fc2.align('P',ezca)
        fc2.align('Y',ezca)

        self.timer['done'] = 5
        #self.counter = 0

    def run(self):

        if not self.timer['done']:
            return

        #if self.counter == 0:
        #    ezca.switch('SUS-FC2_M1_TEST_Y','OFFSET','OFF')
        #    self.counter += 1
        #    self.timer['done'] = 3

        #elif self.counter == 1:
        return True
        
#-------------------------------------------------------------------------------
class LOCK_OPO(GuardState):
    index = 130
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.OPO_node = nodes['SQZ_OPO']

        #ensure OPO node is managed if coming out of SHG_AND_OPO
        self.OPO_node.revive()
        self.OPO_node.set_managed()

        self.ready = False
        self.OPO_locked = False
        self.tempSet = False


        self.counter = 0
        self.failcounter = 0
        self.timer['done'] = 0
        self.timer['stalled'] = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set OPO node to lock with CLF
        if self.counter == 0:
            self.OPO_node.set_request('LOCKED_WITH_CLF')
            self.counter += 1

        elif self.counter == 1:
            if self.OPO_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.OPO_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: OPO loop fail to be locked %d times.'% self.failcounter)
                        
            else:
                notify('waiting for OPO locked')
                self.timer['stalled'] = 2                
                if self.OPO_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True


#-------------------------------------------------------------------------------
class LOCK_CLF(GuardState):
    index = 140
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.CLF_node = nodes['SQZ_CLF']
        self.CLF_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.CLF_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0
	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set CLF node to lock using CLF REFL 2F
        if self.counter == 0:
            self.CLF_node.set_request('CLF_REFL_LOCKED')
            #self.CLF_node.set_request('LOCKED_and_SLOW')
            self.counter += 1

        # Wait for CLF node to arrive, revive if it stalls
        elif self.counter == 1:
            if self.CLF_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.CLF_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: CLF loop fail to be locked %d times.'% self.failcounter)

            else:
                notify('waiting for CLF locked')
                self.timer['stalled'] = 2                
                if self.CLF_node.arrived:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class LOCK_ISS(GuardState):
    index = 150
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_CLF'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.ISS_node = nodes['SQZ_ISS']
        self.ISS_node.revive()
        self.ISS_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0
	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set ISS node to LOCKED, this locks OPO ISS first, then CLF ISS
        if self.counter == 0:
            self.ISS_node.set_request('LOCKED')
            self.counter += 1

        # Wait for ISS node to arrive, revive if it stalls
        elif self.counter == 1:
            if self.ISS_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.ISS_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: ISS loop fail to be locked %d times.'% self.failcounter)

            else:
                notify('waiting for ISS locked')
                self.timer['stalled'] = 2                
                if self.ISS_node.arrived:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class WAIT_IFO_2(GuardState):
    index = 160 #50
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO'])

    def main(self):
        None

    def run(self):
        #JCB fix me! 20250123
        log('Returning True')
        return True
        log('After Returning True')
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump


        return ifo_check()

#------------------------------------------------------------------------------- 
class OPEN_BDIV_2ND(GuardState):
    index = 170
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_CLF', 'SQZ_ISS'])
    
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        if not self.timer['done']:
            return

        if ezca['SQZ-SEED_FLIPPER_CONTROL']:
            notify('close the seed shutter')
            return

        if self.counter == 0:
            ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1
            self.timer['timeout'] = 10
                       
            self.counter += 1
        elif self.counter == 1:
            if not ezca['SYS-MOTION_C_BDIV_E_POSITION'] and not ezca['SYS-MOTION_C_BDIV_E_BUSY']:
                self.counter += 1
            if self.timer['timeout']:
                notify('Can\'t open beam diverter.')
                return

        elif self.counter == 2:
            return True


#-------------------------------------------------------------------------------
class LOCK_FC(GuardState):
    index = 180
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_CLF', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.FC_node = nodes['SQZ_FC']
        self.FC_node.revive()
        #ensure it is managed
        self.FC_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set FC node to LOCKED, 
        if self.counter == 0:
            self.FC_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            if self.FC_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.FC_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: FC loop fail to be locked %d times.'%self.failcounter)

            else:
                notify('waiting for FC locked')
                self.timer['stalled'] = 2                
                if self.FC_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class FC_LOCKED(GuardState):
    index = 300
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG','SQZ_OPO', 'SQZ_CLF', 'SQZ_ISS', 'SQZ_FC'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        return True


#-------------------------------------------------------------------------------
class SWITCH_CLFERR(GuardState):
    index = 500
    request = False

    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO','SQZ_FC'])
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']
        _nodes = NodeManager(['SQZ_LO',])
        self.LO_node = _nodes['SQZ_LO']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.LO_node.STALLED or self.CLF_node.STALLED:
            return 'IDLE'

        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        if not self.LO_node.completed:
            notify('Waiting LO guardian')
            return
        
        if self.counter == 0:
            self.CLF_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            self.LO_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class START_ASCOUTPUT_RECORDING(GuardState):
    index = 600
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_FC'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.SUS_node = nodes['SQZ_SUS']
        self.SUS_node.revive()
        self.SUS_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.counter == 0:
            self.SUS_node.set_request('RECORD_ASC_OUTPUT')
            self.counter += 1

        elif self.counter == 1:
            if self.SUS_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.SUS_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: SUS loop fail to be locked %d times.'% self.failcounter)

            else:
                self.timer['stalled'] = 2                
                if self.SUS_node.completed:
                    self.counter += 1
                

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class DISABLE_SLOWLOOP(GuardState): #what is slowloop
    index = 700
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_ISS']) #?'SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'

    def main(self):
        self.node_CLF = nodes['SQZ_CLF']
        self.node_OPO = nodes['SQZ_OPO']
        self.node_SHG = nodes['SQZ_SHG']
        self.node_PMC = nodes['SQZ_PMC']
        self.node_TTFSS = nodes['SQZ_TTFSS']
        self.counter = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.node_CLF.STALLED:
            self.node_CLF.revive()
            return 'IDLE'
        
        if self.node_SHG.STALLED:
            self.node_SHG.revive()
            return 'IDLE'

        if self.node_PMC.STALLED:
            self.node_PMC.revive()
            return 'IDLE'

        if self.node_TTFSS.STALLED:
            self.node_TTFSS.revive()
            return 'IDLE'



        if self.counter == 0:
            self.node_TTFSS.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            if self.node_TTFSS.completed:
                self.counter += 1

        elif self.counter == 2:
            self.node_PMC.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 3:
            if self.node_PMC.completed:
                self.counter += 1


        if self.counter == 4:
            self.node_SHG.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 5:
            if self.node_SHG.completed:
                self.counter += 1

        elif self.counter == 6:
            self.node_OPO.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 7:
            if self.node_OPO.completed:
                self.counter += 1
                
        elif self.counter == 8:
            return True
   
#-------------------------------------------------------------------------------
class FREQ_DEPENDENT_SQZ(GuardState):
    index = 1000
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS', 'SQZ_FC'])
    

    def main(self):
    
        
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0

        self.FC_failcounter = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        node_FC = nodes['SQZ_FC']
        
        # waiting FC guardian
        if not node_FC.completed:
            notify('Waiting for FC guardian')
            if node_FC.STALLED:
                self.FC_failcounter += 1
                if self.FC_failcounter > 4:
                    notify('FC might have a problem.')
                    return
                node_FC.revive()
            return


        if self.counter == 0:
            # turn off ADF
            #ezca['SQZ-RLF_INTEGRATION_ADFEN'] = 'Off'
            self.counter += 1

        elif self.counter == 1:
            return True

#-------------------------------------------------------------------------------
class SWITCH_FDS_TO_FIS(GuardState):
    index = 171
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS'])
    

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        _nodes = NodeManager(['SQZ_LO',])
        self.LO_node = _nodes['SQZ_LO']
        self.FC_node = nodes['SQZ_FC']


    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        if self.counter == 0:
            # freeze FC ASC
            self.FC_node.set_request('FREEZE_BEAM_CENTERING')
            self.counter += 1

        if self.counter == 1:
            # unlock LO, this will unlock FC
            self.LO_node.set_request('IDLE')
            self.counter += 1
                     
        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class SWITCH_FIS_TO_FDS(GuardState):
    index = 351
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        _nodes = NodeManager(['SQZ_LO',])
        self.LO_node = _nodes['SQZ_LO']
        self.FC_node = nodes['SQZ_FC']


    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        if self.counter == 0:
            # unlock LO
            self.LO_node.set_request('IDLE')
            self.counter += 1
            
        if self.counter == 1:
            # realign FC2
            fc2.align('P',ezca)
            fc2.align('Y',ezca)
            self.counter += 1
            
        if self.counter == 2:
            # unfreeze FC ASC
            self.FC_node.set_request('UNFREEZE_BEAM_CENTERING')
            self.counter += 1

        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------
class PREP_SCAN(GuardState):
    index = 950
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS', 'SQZ_FC'])
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        

        if self.CLF_node.STALLED:
            return 'IDLE'

        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        if self.counter == 0:
            # lock with clf refl
            self.CLF_node.set_request('CLF_REFL_LOCKED')
            self.counter += 1

        elif self.counter == 1:
            # disable SQZASC
            for dof in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-ASC_%s_%s'%(dof,dof2),'INPUT','OFF')

            self.counter += 1
            self.timer['done'] = 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class FDS_QNOISE_SCAN(GuardState):
    index = 995
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        self.dur = 5*60
        self.repeat = 2
        self.freq = 1/60 #1/(300), 1/60
        self.cycle = 1
        self.SQZANG_amp = 20 #100 
        self.SQZANG_DC = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']
        self.angles = np.array([self.SQZANG_DC, 150, 170, 180, 205, 140, 90, 60])
#            sqz_angle = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

        self.timer['done'] = 0
        self.counter = 0
        self.angle_counter = 0
        self.repeat_counter = 0

        self.dirname = '/data/SQZ/GRD/FDS_QNOISE_SCAN/'
        now = datetime.datetime.now()
        #self.dirname = '/data/SQZ/GRD/FDS_QNOISE_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
        #os.mkdir(self.dirname)

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
        
            self.scan_start = gpstime.gpsnow() + 1
            self.counter += 1
            self.data = [[],[],[],[],[]]
            self.start_time = 0
            self.end_time = 0
            

        elif self.counter == 1:
            # scan SQZANG
            for j in range(self.repeat):
            
                for i in range(len(self.angles)):
                    self.start_time = gpstime.gpsnow() + 1
                    #log(self.angles[i])
                    ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = self.angles[i]
                    time.sleep(self.dur)
                    self.angle_counter += 1
                    self.end_time = gpstime.gpsnow() + 1
                    self.data[0].append(self.start_time)
                    self.data[1].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                    self.data[2].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                    self.data[3].append(ezca['SQZ-DCPD_RATIO_4_DB_MON'])
                    self.data[4].append(self.end_time)
                    
#                    notify('CLF angle %d sec'%(self.angles[i]))
                
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = self.angles[0]
                self.repeat_counter += 1
#                log(self.repeat_counter)
                
            self.counter += 1
#            log(self.counter)
            
            
        elif self.counter == 2:
            log('Scan done, revert angle, save')
            ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = self.angles[0]
            now = datetime.datetime.now()
            filename = 'FDS_QNOISE_SCAN_%s.txt'%now.strftime('%y%m%d%H%M%S')
            transposed_data = list(zip(*self.data))
            
            with open(self.dirname + filename, 'w') as file:
                file.write('start time, CLF angle, OMC trans RF3 abs, DCPD ratio 4 dB, end time \n')
                for row in transposed_data:
                    file.write(str(row)+'\n')
            
            self.counter += 1
        
                
        elif self.counter == 3:
            log('Data saved, all done')
            return True


#-------------------------------------------------------------------------------
class SCAN_SQZANG(GuardState):
    index = 960
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])
    
    def calc_local_minimum(self,k):
        derivative = np.polyder(k)
        extremum_arg = np.roots(derivative)
        extremum_val = np.polyval(k,extremum_arg)

        local_min = min(extremum_val)
        local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
        return local_min, local_min_arg
            
    def fit_SQZANG(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])
            
        
        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        SQZANG = self.data[0]
        OMCQ = self.data[1]
        db = self.data[2]

        # fit
        fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
        fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)        
        best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
        best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)        

        best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
        best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20
        
        min_sqz = min(db)
        fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
        fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20        

        fig.suptitle('Squeezing angle scan')
        axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
        axs[0].set_xlabel('CLF phase [deg]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
        axs[1].set_xlabel('OMCQ')
        axs[1].set_ylabel('SQZ [dB]')
        axs[1].grid()
        #axs[1].set_ylim([-7,-1])
        axs[1].legend()
        
        now = datetime.datetime.now()
        filename = 'SQZANG_SCAN_%s.png'%now.strftime('%y%m%d%H%M%S')
        fig.savefig(self.dirname+filename)
        return best_SQZANG, best_OMCQ
    

    def main(self):
        self.freq = 1/60 #1/(300), 1/60
        self.cycle = 1
        self.SQZANG_amp = 20 #100 
        self.SQZANG_DC = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

        self.timer['done'] = 0
        self.counter = 0

        self.dirname = '/data/SQZ/GRD/SQZANG_SCAN/'

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
        
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]
            
        elif self.counter == 1:
            # scan SQZANG
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                           self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.SQZANG_DC, log=False)
                self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('SQZ angle scanning %d sec/%d sec'%(now-self.scan_start,1/self.freq))
                
        elif self.counter == 2:
            # fit scan result
            best_ang, best_OMCQ = self.fit_SQZANG()
            ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
            ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
            self.timer['done'] = 2
            self.counter += 1
                
        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------
class SCAN_ALIGNMENT(GuardState):
    index = 965
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC']) #O4a
   # DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS'])
    
    def calc_local_minimum(self,k):
        derivative = np.polyder(k)
        extremum_arg = np.roots(derivative)
        extremum_val = np.polyval(k,extremum_arg)

        local_min = min(extremum_val)
        local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
        return local_min, local_min_arg
            
    def fit(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])

        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        ofs_sus = {'ZM4':self.data[0]/self.OPTAL['ZM4'+self.dof].GAIN.get(),'ZM6':self.data[1]/self.OPTAL['ZM6'+self.dof].GAIN.get()}
        ofs = ofs_sus['ZM4'] - self.DC['ZM4'+self.dof]
        db = self.data[2]

        # fit
        fit_pol = np.polyfit(ofs,10**(db/20),3)
        best_DCPD_ratio, best_ofs = self.calc_local_minimum(fit_pol)

        best_ofs_ZM4 = self.DC['ZM4'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM4']
        best_ofs_ZM6 = self.DC['ZM6'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM6']            

        
        best_sqz = np.log10(best_DCPD_ratio)*20
        min_sqz = min(db)
        fit_db = np.log10(np.polyval(fit_pol,ofs))*20

        fig.suptitle(self.comdif + ' ' + self.dof)
        axs[0].plot(ofs,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(ofs,fit_db,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz,best_ofs))
        axs[0].set_xlabel('OPTIC ALIGN offset [urad]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        
        mappable = axs[1].scatter(ofs_sus['ZM4'],ofs_sus['ZM6'],c=db)
        axs[1].plot(best_ofs_ZM4,best_ofs_ZM6,'r*',markersize=30)
        fig.colorbar(mappable,ax=axs[1])
        axs[1].set_xlabel('ZM4_%s OPTIC ALIGN [urad]'%(self.dof))
        axs[1].set_ylabel('ZM6_%s OPTIC ALIGN [urad]'%(self.dof))
        filename = '%s_%s_SCAN.png'%(self.comdif,self.dof)
        fig.savefig(self.dirname+filename)
        return best_ofs_ZM4, best_ofs_ZM6

    def fit_SQZANG(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])
            
        
        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        SQZANG = self.data[0]
        OMCQ = self.data[1]
        db = self.data[2]

        # fit
        fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
        fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)        
        best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
        best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)        

        best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
        best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20
        
        min_sqz = min(db)
        fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
        fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20        

        fig.suptitle('Squeezing angle scan')
        axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
        axs[0].set_xlabel('CLF phase [deg]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
        axs[1].set_xlabel('OMCQ')
        axs[1].set_ylabel('SQZ [dB]')
        axs[1].grid()
        #axs[1].set_ylim([-7,-1])
        axs[1].legend()

        filename = 'SQZANG_SCAN_%d.png'%self.ANGSCAN_idx
        fig.savefig(self.dirname+filename)
        return best_SQZANG, best_OMCQ
    

    def main(self):
        self.amp = {'DIF':70,'COM':150}
        self.freq = 1/60
        self.cycle = 1

        self.SQZANG_amp = 30

        self.dof = 'P'
        self.comdif = 'DIF'
        
        # initialization
        self.direction = {'P':{'DIF':{'ZM4':0.94,'ZM6':-0.34},'COM':{'ZM4':0.34,'ZM6':0.94}},
                          'Y':{'DIF':{'ZM4':0.67,'ZM6':-0.74},'COM':{'ZM4':0.74,'ZM6':0.67}}}
        self.OPTAL = {}
        self.DC = {}
                
        self.timer['done'] = 0
        self.counter = 0

        now = datetime.datetime.now()
        self.dirname = '/data/SQZ/GRD/ZM_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
        os.mkdir(self.dirname)

        self.ANGSCAN_idx = 0

        self.test = False

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
            for sus in ['ZM4','ZM6']:
                for dof in ['P','Y']:
                    self.OPTAL[sus+dof] = ezca.get_LIGOFilter('SUS-%s_M1_OPTICALIGN_%s'%(sus,dof))
                    self.DC[sus+dof] = self.OPTAL[sus+dof].OFFSET.get()
                    self.OPTAL[sus+dof].TRAMP.put(0.1)
            self.DC['SQZANG'] = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']
            
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]

        elif self.counter == 1:                   
            # scan suspension
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                for sus in ['ZM4','ZM6']:                    
                    self.OPTAL[sus+self.dof].OFFSET.put(
                        self.direction[self.dof][self.comdif][sus]*self.amp[self.comdif]*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC[sus+self.dof], log=False)
                
                self.data[0].append(ezca['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[1].append(ezca['SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('%s %s scanning %d sec/%d sec (%d/8)'%(self.comdif,self.dof,now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+1))
                
        elif self.counter == 2:
            # fit suspension scan result
            notify('fetching %s data'%self.dof)
            '''
            data = cdsutils.getdata(['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
                                      'SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
                                      'SQZ-DCPD_RATIO_5_DB_MON'],
                                     self.scan_end-self.scan_start, self.scan_start)
            '''
            zm4_ofs, zm6_ofs = self.fit()
            self.OPTAL['ZM4'+self.dof].TRAMP.put(3)
            self.OPTAL['ZM6'+self.dof].TRAMP.put(3)
            time.sleep(0.5)
            if not self.test:
                self.OPTAL['ZM4'+self.dof].OFFSET.put(zm4_ofs)
                self.OPTAL['ZM6'+self.dof].OFFSET.put(zm6_ofs)
            
            self.timer['done'] = 5            
            self.counter += 1

        elif self.counter == 3:
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]
            
        elif self.counter == 4:
            # scan SQZANG
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                           self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC['SQZANG'], log=False)
                self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('SQZ angle scanning %d sec/%d sec (%d/8)'%(now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+2))
                

        elif self.counter == 5:
            # fit scan result
            notify('fetching SQZ angle scan data')
            '''data = cdsutils.getdata(['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                                     'SQZ-OMC_TRANS_RF3_Q_MON',
                                     'SQZ-DCPD_RATIO_5_DB_MON'],
                                     self.scan_end-self.scan_start, self.scan_start)'''
            self.ANGSCAN_idx += 1
            best_ang, best_OMCQ = self.fit_SQZANG()
            if not self.test:
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
                ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
            self.timer['done'] = 5            
            self.counter += 1
                

        elif self.counter == 6:
            if self.dof == 'P':
                self.dof = 'Y'
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 7:
            if self.comdif == 'DIF':
                self.comdif = 'COM'
                self.dof = 'P'                
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 8:            
            return True

#-------------------------------------------------------------------------------
class ZERO_AS42_OFFSET(GuardState):
    index = 970
    request = False
    DEPS = frozenset(['SQZ_SUS',  'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO','SQZ_ISS', 'SQZ_FC'])

    def append_cdsdata(self,vals,val):
        if not vals[-1] == val:
            return np.append(vals,val)
        else:
            return vals

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        self.avgT = 600
        self.timer['avg'] = self.avgT
        self.start = gpstime.gpsnow()

        self.val_dict = {}
        for dof in ['PIT','YAW']:
            for wfs in ['A','B']:
                self.val_dict[dof+wfs] = np.array([ezca['ASC-AS_%s_RF42_%s_INMON'%(wfs,dof)]])
                

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        

        if not self.timer['avg']:
            for dof in ['PIT','YAW']:
                for wfs in ['A','B']:
                    self.val_dict[dof+wfs] = self.append_cdsdata(self.val_dict[dof+wfs],
                                                                 ezca['ASC-AS_%s_RF42_%s_INMON'%(wfs,dof)])
            now = gpstime.gpsnow()
            dT = now-self.start
            notify('Averaging... [%d sec / %d sec]'%(dT,self.avgT))            

        else:
            for dof in ['PIT','YAW']:
                for wfs in ['A','B']:
                    ezca['ASC-AS_%s_RF42_%s_OFFSET'%(wfs,dof)] = -np.mean(self.val_dict[dof+wfs])
                    
            return True
                    
#-------------------------------------------------------------------------------
class SQZ_ALIGNED(GuardState):
    index = 980
    request = True
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC','SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC','SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        ezca['GRD-SQZ_MANAGER_REQUEST'] = 'FREQ_DEPENDENT_SQZ'

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        else: return True

#-------------------------------------------------------------------------------
class SQZANG_OPTIMIZED(GuardState):
    index = 981
    request = True
    #DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        ezca['GRD-SQZ_MANAGER_REQUEST'] = 'FREQ_DEPENDENT_SQZ'        

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        else: return True

#-------------------------------------------------------------------------------
class REVERT(GuardState):
    index = 985
    request = False
    DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 1 #0 #AJM20240603 set to 1 to skip CLF request below, as we aren't currently going to this state.
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        

        if self.CLF_node.STALLED:
            return 'IDLE'

        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        
        if self.counter == 0:
            # lock with clf refl
            self.CLF_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            # enable SQZASC
            #for dof in ['POS','ANG']:
            #    for dof2 in ['P','Y']:
            #        ezca.switch('SQZ-ASC_%s_%s'%(dof,dof2),'INPUT','ON')

            self.counter += 1
            self.timer['done'] = 1

        elif self.counter == 2:
            return True



#-------------------------------------------------------------------------------
'''
class PREP_SCAN2(GuardState):
    index = 9502
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS', 'SQZ_FC'])
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO',  'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        

        if self.CLF_node.STALLED:
            return 'IDLE'

        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        if self.counter == 0:
            # lock with clf refl
            self.CLF_node.set_request('CLF_REFL_LOCKED')
            self.counter += 1

        elif self.counter == 1:
            # disable SQZASC
            for dof in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-ASC_%s_%s'%(dof,dof2),'INPUT','OFF')

            self.counter += 1
            self.timer['done'] = 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class SCAN_SQZANG2(GuardState):
    index = 9602
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])
    
    def calc_local_minimum(self,k):
        derivative = np.polyder(k)
        extremum_arg = np.roots(derivative)
        extremum_val = np.polyval(k,extremum_arg)

        local_min = min(extremum_val)
        local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
        return local_min, local_min_arg
            
    def fit_SQZANG(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])
            
        
        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        SQZANG = self.data[0]
        OMCQ = self.data[1]
        db = self.data[2]

        # fit
        fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
        fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)        
        best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
        best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)        

        best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
        best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20
        
        min_sqz = min(db)
        fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
        fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20        

        fig.suptitle('Squeezing angle scan')
        axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
        axs[0].set_xlabel('CLF phase [deg]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
        axs[1].set_xlabel('OMCQ')
        axs[1].set_ylabel('SQZ [dB]')
        axs[1].grid()
        #axs[1].set_ylim([-7,-1])
        axs[1].legend()
        
        now = datetime.datetime.now()
        filename = 'SQZANG_SCAN_%s.png'%now.strftime('%y%m%d%H%M%S')
        fig.savefig(self.dirname+filename)
        return best_SQZANG, best_OMCQ
    

    def main(self):
        self.freq = 1/60
        self.cycle = 1
        self.SQZANG_amp = 30
        self.SQZANG_DC = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']

        self.timer['done'] = 0
        self.counter = 0

        self.dirname = '/data/SQZ/GRD/SQZANG_SCAN/'

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
        
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]
            
        elif self.counter == 1:
            # scan SQZANG
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                           self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.SQZANG_DC, log=False)
                self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('SQZ angle scanning %d sec/%d sec'%(now-self.scan_start,1/self.freq))
                
        elif self.counter == 2:
            # fit scan result
            best_ang, best_OMCQ = self.fit_SQZANG()
            ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
            ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
            self.timer['done'] = 2
            self.counter += 1
                
        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------

class SCAN_ALIGNMENT2(GuardState):
    index = 9652
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC']) #O4a
    DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS'])
    
    def calc_local_minimum(self,k):
        derivative = np.polyder(k)
        extremum_arg = np.roots(derivative)
        extremum_val = np.polyval(k,extremum_arg)

        local_min = min(extremum_val)
        local_min_arg = extremum_arg[list(extremum_val).index(local_min)]
        return local_min, local_min_arg
            
    def fit(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])

        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        ofs_sus = {'ZM4':self.data[0]/self.OPTAL['ZM4'+self.dof].GAIN.get(),'ZM6':self.data[1]/self.OPTAL['ZM6'+self.dof].GAIN.get()}
        ofs = ofs_sus['ZM4'] - self.DC['ZM4'+self.dof]
        db = self.data[2]

        # fit
        fit_pol = np.polyfit(ofs,10**(db/20),3)
        best_DCPD_ratio, best_ofs = self.calc_local_minimum(fit_pol)

        best_ofs_ZM4 = self.DC['ZM4'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM4']
        best_ofs_ZM6 = self.DC['ZM6'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM6']            

        
        best_sqz = np.log10(best_DCPD_ratio)*20
        min_sqz = min(db)
        fit_db = np.log10(np.polyval(fit_pol,ofs))*20

        fig.suptitle(self.comdif + ' ' + self.dof)
        axs[0].plot(ofs,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(ofs,fit_db,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz,best_ofs))
        axs[0].set_xlabel('OPTIC ALIGN offset [urad]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        
        mappable = axs[1].scatter(ofs_sus['ZM4'],ofs_sus['ZM6'],c=db)
        axs[1].plot(best_ofs_ZM4,best_ofs_ZM6,'r*',markersize=30)
        fig.colorbar(mappable,ax=axs[1])
        axs[1].set_xlabel('ZM4_%s OPTIC ALIGN [urad]'%(self.dof))
        axs[1].set_ylabel('ZM6_%s OPTIC ALIGN [urad]'%(self.dof))
        filename = '%s_%s_SCAN.png'%(self.comdif,self.dof)
        fig.savefig(self.dirname+filename)
        return best_ofs_ZM4, best_ofs_ZM6

    def fit_SQZANG(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])
            
        
        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        SQZANG = self.data[0]
        OMCQ = self.data[1]
        db = self.data[2]

        # fit
        fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
        fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)        
        best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_minimum(fit_pol_SQZANG)
        best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_minimum(fit_pol_OMCQ)        

        best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
        best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20
        
        min_sqz = min(db)
        fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
        fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20        

        fig.suptitle('Squeezing angle scan')
        axs[0].plot(SQZANG,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
        axs[0].set_xlabel('CLF phase [deg]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        axs[1].plot(OMCQ,db,'*',label='meas: minsqz=%.2f'%(min_sqz))
        axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
        axs[1].set_xlabel('OMCQ')
        axs[1].set_ylabel('SQZ [dB]')
        axs[1].grid()
        #axs[1].set_ylim([-7,-1])
        axs[1].legend()

        filename = 'SQZANG_SCAN_%d.png'%self.ANGSCAN_idx
        fig.savefig(self.dirname+filename)
        return best_SQZANG, best_OMCQ
    

    def main(self):
        self.amp = {'DIF':70,'COM':150}
        self.freq = 1/60
        self.cycle = 1

        self.SQZANG_amp = 30

        self.dof = 'P'
        self.comdif = 'DIF'
        
        # initialization
        self.direction = {'P':{'DIF':{'ZM4':0.94,'ZM6':-0.34},'COM':{'ZM4':0.34,'ZM6':0.94}},
                          'Y':{'DIF':{'ZM4':0.67,'ZM6':-0.74},'COM':{'ZM4':0.74,'ZM6':0.67}}}
        self.OPTAL = {}
        self.DC = {}
                
        self.timer['done'] = 0
        self.counter = 0

        now = datetime.datetime.now()
        self.dirname = '/data/SQZ/GRD/ZM_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
        os.mkdir(self.dirname)

        self.ANGSCAN_idx = 0

        self.test = False

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
            for sus in ['ZM4','ZM6']:
                for dof in ['Y','P']:
                    self.OPTAL[sus+dof] = ezca.get_LIGOFilter('SUS-%s_M1_OPTICALIGN_%s'%(sus,dof))
                    self.DC[sus+dof] = self.OPTAL[sus+dof].OFFSET.get()
                    self.OPTAL[sus+dof].TRAMP.put(0.1)
            self.DC['SQZANG'] = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']
            
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]

        elif self.counter == 1:                   
            # scan suspension
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                for sus in ['ZM4','ZM6']:                    
                    self.OPTAL[sus+self.dof].OFFSET.put(
                        self.direction[self.dof][self.comdif][sus]*self.amp[self.comdif]*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC[sus+self.dof], log=False)
                
                self.data[0].append(ezca['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[1].append(ezca['SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('%s %s scanning %d sec/%d sec (%d/8)'%(self.comdif,self.dof,now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+1))
                
        elif self.counter == 2:
            # fit suspension scan result
            notify('fetching %s data'%self.dof)
            
            data = cdsutils.getdata(['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
                                      'SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
                                      'SQZ-DCPD_RATIO_5_DB_MON'],
                                     self.scan_end-self.scan_start, self.scan_start)
            
            zm4_ofs, zm6_ofs = self.fit()
            self.OPTAL['ZM4'+self.dof].TRAMP.put(3)
            self.OPTAL['ZM6'+self.dof].TRAMP.put(3)
            time.sleep(0.5)
            if not self.test:
                self.OPTAL['ZM4'+self.dof].OFFSET.put(zm4_ofs)
                self.OPTAL['ZM6'+self.dof].OFFSET.put(zm6_ofs)
            
            self.timer['done'] = 5            
            self.counter += 1

        elif self.counter == 3:
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]
            
        elif self.counter == 4:
            # scan SQZANG
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                           self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC['SQZANG'], log=False)
                self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('SQZ angle scanning %d sec/%d sec (%d/8)'%(now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+2))
                

        elif self.counter == 5:
            # fit scan result
            notify('fetching SQZ angle scan data')
            #data = cdsutils.getdata(['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
            #                         'SQZ-OMC_TRANS_RF3_Q_MON',
            #                         'SQZ-DCPD_RATIO_5_DB_MON'],
            #                         self.scan_end-self.scan_start, self.scan_start)
            self.ANGSCAN_idx += 1
            best_ang, best_OMCQ = self.fit_SQZANG()
            if not self.test:
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
                ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
            self.timer['done'] = 5            
            self.counter += 1
                

        elif self.counter == 6:
            if self.dof == 'P':
                self.dof = 'Y'
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 7:
            if self.comdif == 'DIF':
                self.comdif = 'COM'
                self.dof = 'P'                
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 8:            
            return True
            
            
#-------------------------------------------------------------------------------
class SCAN_ALIGNMENT3(GuardState):
    index = 9653
    request = False
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC']) #O4a
    DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS'])
    
    def calc_local_maximum(self,k):
        derivative = np.polyder(k)
        extremum_arg = np.roots(derivative)
        extremum_val = np.polyval(k,extremum_arg)

        local_max = max(extremum_val)
        local_max_arg = extremum_arg[list(extremum_val).index(local_max)]
        return local_max, local_max_arg
            
    def fit(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])

        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        ofs_sus = {'ZM4':self.data[0]/self.OPTAL['ZM4'+self.dof].GAIN.get(),'ZM6':self.data[1]/self.OPTAL['ZM6'+self.dof].GAIN.get()}
        ofs = ofs_sus['ZM4'] - self.DC['ZM4'+self.dof]
        db = self.data[2]

        # fit
        fit_pol = np.polyfit(ofs,10**(db/20),3)
        best_DCPD_ratio, best_ofs = self.calc_local_maximum(fit_pol)

        best_ofs_ZM4 = self.DC['ZM4'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM4']
        best_ofs_ZM6 = self.DC['ZM6'+self.dof] + best_ofs * self.direction[self.dof][self.comdif]['ZM6']            

        
        best_sqz = np.log10(best_DCPD_ratio)*20
        max_sqz = max(db)
        fit_db = np.log10(np.polyval(fit_pol,ofs))*20

        fig.suptitle(self.comdif + ' ' + self.dof)
        axs[0].plot(ofs,db,'*',label='meas: maxsqz=%.2f'%(max_sqz))
        axs[0].plot(ofs,fit_db,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz,best_ofs))
        axs[0].set_xlabel('OPTIC ALIGN offset [urad]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        
        mappable = axs[1].scatter(ofs_sus['ZM4'],ofs_sus['ZM6'],c=db)
        axs[1].plot(best_ofs_ZM4,best_ofs_ZM6,'r*',markersize=30)
        fig.colorbar(mappable,ax=axs[1])
        axs[1].set_xlabel('ZM4_%s OPTIC ALIGN [urad]'%(self.dof))
        axs[1].set_ylabel('ZM6_%s OPTIC ALIGN [urad]'%(self.dof))
        filename = '%s_%s_SCAN.png'%(self.comdif,self.dof)
        fig.savefig(self.dirname+filename)
        return best_ofs_ZM4, best_ofs_ZM6

    def fit_SQZANG(self):
        for ii in range(len(self.data)):
            self.data[ii] = np.array(self.data[ii])
            
        
        fig, axs = plt.subplots(1,2,figsize=(13,5))        
        SQZANG = self.data[0]
        OMCQ = self.data[1]
        db = self.data[2]

        # fit
        fit_pol_SQZANG = np.polyfit(SQZANG,10**(db/20),3)
        fit_pol_OMCQ = np.polyfit(OMCQ,10**(db/20),3)        
        best_DCPD_ratio_SQZANG, best_SQZANG = self.calc_local_maximum(fit_pol_SQZANG)
        best_DCPD_ratio_OMCQ, best_OMCQ = self.calc_local_maximum(fit_pol_OMCQ)        

        best_sqz_SQZANG = np.log10(best_DCPD_ratio_SQZANG)*20
        best_sqz_OMCQ = np.log10(best_DCPD_ratio_OMCQ)*20
        
        min_sqz = min(db)
        fit_db_SQZANG = np.log10(np.polyval(fit_pol_SQZANG,SQZANG))*20
        fit_db_OMCQ = np.log10(np.polyval(fit_pol_OMCQ,OMCQ))*20        

        fig.suptitle('Squeezing angle scan')
        axs[0].plot(SQZANG,db,'*',label='meas: maxsqz=%.2f'%(min_sqz))
        axs[0].plot(SQZANG,fit_db_SQZANG,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_SQZANG,best_SQZANG))
        axs[0].set_xlabel('CLF phase [deg]')
        axs[0].set_ylabel('SQZ [dB]')
        axs[0].grid()
        #axs[0].set_ylim([-7,-1])
        axs[0].legend()

        axs[1].plot(OMCQ,db,'*',label='meas: maxsqz=%.2f'%(min_sqz))
        axs[1].plot(OMCQ,fit_db_OMCQ,label='fit: bestsqz=%.2f @ %.2f'%(best_sqz_OMCQ,best_OMCQ))
        axs[1].set_xlabel('OMCQ')
        axs[1].set_ylabel('SQZ [dB]')
        axs[1].grid()
        #axs[1].set_ylim([-7,-1])
        axs[1].legend()

        filename = 'SQZANG_SCAN_%d.png'%self.ANGSCAN_idx
        fig.savefig(self.dirname+filename)
        return best_SQZANG, best_OMCQ
    

    def main(self):
        self.amp = {'DIF':70,'COM':150}
        self.freq = 1/60
        self.cycle = 1

        self.SQZANG_amp = 30

        self.dof = 'P'
        self.comdif = 'DIF'
        
        # initialization
        self.direction = {'P':{'DIF':{'ZM4':0.94,'ZM6':-0.34},'COM':{'ZM4':0.34,'ZM6':0.94}},
                          'Y':{'DIF':{'ZM4':0.67,'ZM6':-0.74},'COM':{'ZM4':0.74,'ZM6':0.67}}}
        self.OPTAL = {}
        self.DC = {}
                
        self.timer['done'] = 0
        self.counter = 0

        now = datetime.datetime.now()
        self.dirname = '/data/SQZ/GRD/ZM_SCAN/%s/'%now.strftime('%y%m%d%H%M%S')
        os.mkdir(self.dirname)

        self.ANGSCAN_idx = 0

        self.test = False

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if not self.timer['done']:
            return

        if self.counter == 0:
            for sus in ['ZM4','ZM6']:
                for dof in ['Y','P']:
                    self.OPTAL[sus+dof] = ezca.get_LIGOFilter('SUS-%s_M1_OPTICALIGN_%s'%(sus,dof))
                    self.DC[sus+dof] = self.OPTAL[sus+dof].OFFSET.get()
                    self.OPTAL[sus+dof].TRAMP.put(0.1)
            self.DC['SQZANG'] = ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG']
            
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]

        elif self.counter == 1:                   
            # scan suspension
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                for sus in ['ZM4','ZM6']:                    
                    self.OPTAL[sus+self.dof].OFFSET.put(
                        self.direction[self.dof][self.comdif][sus]*self.amp[self.comdif]*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC[sus+self.dof], log=False)
                
                self.data[0].append(ezca['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[1].append(ezca['SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('%s %s scanning %d sec/%d sec (%d/8)'%(self.comdif,self.dof,now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+1))
                
        elif self.counter == 2:
            # fit suspension scan result
            notify('fetching %s data'%self.dof)

            #data = cdsutils.getdata(['SUS-ZM4_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
            #                          'SUS-ZM6_M1_OPTICALIGN_%s_OUTPUT'%self.dof,
            #                          'SQZ-DCPD_RATIO_5_DB_MON'],
            #                         self.scan_end-self.scan_start, self.scan_start)
            
            zm4_ofs, zm6_ofs = self.fit()
            self.OPTAL['ZM4'+self.dof].TRAMP.put(3)
            self.OPTAL['ZM6'+self.dof].TRAMP.put(3)
            time.sleep(0.5)
            if not self.test:
                self.OPTAL['ZM4'+self.dof].OFFSET.put(zm4_ofs)
                self.OPTAL['ZM6'+self.dof].OFFSET.put(zm6_ofs)
            
            self.timer['done'] = 5            
            self.counter += 1

        elif self.counter == 3:
            self.scan_start = gpstime.gpsnow() + 1
            self.timer['scan'] = 1/self.freq * self.cycle + 1
            self.timer['done'] = 1
            self.counter += 1
            self.data = [[],[],[]]
            
        elif self.counter == 4:
            # scan SQZANG
            if self.timer['scan']:
                self.scan_end = gpstime.gpsnow()
                self.counter += 1

            else:
                now = gpstime.gpsnow()
                ezca.write('SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                           self.SQZANG_amp*np.sin((now-self.scan_start)*self.freq*2*np.pi) + self.DC['SQZANG'], log=False)
                self.data[0].append(ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'])
                self.data[1].append(ezca['SQZ-OMC_TRANS_RF3_Q_MON'])
                self.data[2].append(ezca['SQZ-DCPD_RATIO_5_DB_MON'])
                notify('SQZ angle scanning %d sec/%d sec (%d/8)'%(now-self.scan_start,1/self.freq,self.ANGSCAN_idx*2+2))
                

        elif self.counter == 5:
            # fit scan result
            notify('fetching SQZ angle scan data')
            #data = cdsutils.getdata(['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
            #                         'SQZ-OMC_TRANS_RF3_Q_MON',
            #                         'SQZ-DCPD_RATIO_5_DB_MON'],
            #                         self.scan_end-self.scan_start, self.scan_start)
            self.ANGSCAN_idx += 1
            best_ang, best_OMCQ = self.fit_SQZANG()
            if not self.test:
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = best_ang
                ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'] = best_OMCQ
            self.timer['done'] = 5            
            self.counter += 1
                

        elif self.counter == 6:
            if self.dof == 'P':
                self.dof = 'Y'
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 7:
            if self.comdif == 'DIF':
                self.comdif = 'COM'
                self.dof = 'P'                
                self.counter = 0                
            else:
                self.counter += 1

        elif self.counter == 8:            
            return True


#-------------------------------------------------------------------------------

class SQZ_ALIGNED2(GuardState):
    index = 9802
    request = True
    #DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC','SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC','SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        #ezca['GRD-SQZ_MANAGER_REQUEST'] = 'FREQ_DEPENDENT_SQZ'

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        else: return True

#-------------------------------------------------------------------------------
class SQZANG_OPTIMIZED2(GuardState):
    index = 9812
    request = True
    #DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS', 'SQZ_FC'])
    DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        #ezca['GRD-SQZ_MANAGER_REQUEST'] = 'FREQ_DEPENDENT_SQZ'        

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        else: return True

#-------------------------------------------------------------------------------
class REVERT2(GuardState):
    index = 986
    request = False
    DEPS = frozenset(['SQZ_SUS','SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_ISS'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        

        if self.CLF_node.STALLED:
            return 'IDLE'

        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        
        if self.counter == 0:
            # lock with clf refl
            #self.CLF_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            # enable SQZASC
            #for dof in ['POS','ANG']:
            #    for dof2 in ['P','Y']:
            #        ezca.switch('SQZ-ASC_%s_%s'%(dof,dof2),'INPUT','ON')

            self.counter += 1
            self.timer['done'] = 1

        elif self.counter == 2:
            return True
'''

#######################
class MISALIGN_FC2_2ND(GuardState):
    index = 185
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG'])
    
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        #ezca['SUS-FC2_M1_TEST_Y_TRAMP'] = 3
        #ezca['SUS-FC2_M1_TEST_Y_OFFSET'] = 1000

        fc2.misalign('Y',1000,ezca)

        self.timer['done'] = 5 #0
        #self.counter = 0

    def run(self):

        if not self.timer['done']:
            return

        #if self.counter == 0:
        #    ezca.switch('SUS-FC2_M1_TEST_Y','OFFSET','ON')
        #    self.counter += 1
        #    self.timer['done'] = 3

        #elif self.counter == 1:

        return True

#-------------------------------------------------------------------------------
class LOCK_LO(GuardState):
    index = 190
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_CLF'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        self.LO_node = nodes['SQZ_LO']
        self.LO_node.revive()
        #ensure it is managed if coming out of SHG_AND_OPO
        self.LO_node.set_managed()
        
        self.timer['stalled'] = 2            
        self.counter = 0
        self.failcounter = 0
	

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        # Set LO node to lock 
        if self.counter == 0:
            self.LO_node.set_request('LOCKED')
            self.counter += 1

        # Wait for LO node to arrive, revive if it stalls
        elif self.counter == 1:
            if self.LO_node.STALLED:
                if self.timer['stalled']:
                    self.failcounter += 1
                    self.LO_node.revive()
                    if self.failcounter > 4:
                        notify('Warning: LO loop fail to be locked %d times.'% self.failcounter)

            else:
                notify('waiting for LO locked')
                self.timer['stalled'] = 2                
                if self.LO_node.arrived:
                    self.counter += 1
                

        elif self.counter == 2:
            return True
        
#-------------------------------------------------------------------------------
class FREQ_INDEPENDENT_SQZ(GuardState):
    index = 350
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_TTFSS', 'SQZ_PMC', 'SQZ_SHG','SQZ_OPO', 'SQZ_CLF', 'SQZ_ISS', 'SQZ_LO'])

    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        return True
    
##################################
############# GRAPH ##############
##################################

edges = [
    ('INIT', 'IDLE'),
    ('IDLE','RESET'),
    ('RESET', 'SERVO_PSAMS'),
    ('SERVO_PSAMS','LOCK_TTFSS'),
    ('LOCK_TTFSS','LOCK_PMC'),
    ('LOCK_PMC','LOCK_SHG'),
    ('LOCK_SHG','ADJUST_GR_POL'),
    ('LOCK_SHG','WAIT_IFO_1'),
    ('WAIT_IFO_1', 'MISALIGN_FC2_1ST'),
    ('MISALIGN_FC2_1ST','LOCK_SINGLE_CLF'),
    ('LOCK_SINGLE_CLF','OPEN_BDIV_1ST'),
    ('OPEN_BDIV_1ST','SINGLE_CLF_ADS'),    
    ('LOCK_OPO','LOCK_CLF'),
    ('LOCK_CLF','LOCK_ISS'),
    ('LOCK_ISS','WAIT_IFO_2'),
    ('WAIT_IFO_2','OPEN_BDIV_2ND'),
    ('OPEN_BDIV_2ND','LOCK_FC'),
    ('LOCK_FC','FC_LOCKED'),
    #('FC_LOCKED','SWITCH_CLFERR'),
    #('SWITCH_CLFERR','START_ASCOUTPUT_RECORDING'),
    ('FC_LOCKED','START_ASCOUTPUT_RECORDING'),
    ('START_ASCOUTPUT_RECORDING','DISABLE_SLOWLOOP'),
    ('DISABLE_SLOWLOOP','FREQ_DEPENDENT_SQZ'),


    #('OPEN_BDIV_2ND','PREP_SCAN2'),
    #('PREP_SCAN2','SCAN_ALIGNMENT2'),
    #('SCAN_ALIGNMENT2','SQZ_ALIGNED2'),
    #('SQZ_ALIGNED2','SCAN_SQZANG2'),
    #('SCAN_SQZANG2','SQZANG_OPTIMIZED2'),
    #('SQZ_ALIGNED2','REVERT2'),
    #('REVERT2','OPEN_BDIV_2ND'),
    #('SQZANG_OPTIMIZED2','REVERT2'),
    #('REVERT2','OPEN_BDIV_2ND'),
    #('PREP_SCAN2','SCAN_ALIGNMENT3',1),
 
#    ('WAIT_IFO_2','FDS_QNOISE_SCAN'), #Begum, 2025-02-04, just trial purposes
#    ('FDS_QNOISE_SCAN','WAIT_IFO_2'), #Begum, 2025-02-04, just trial purposes

    ('FREQ_DEPENDENT_SQZ','FDS_QNOISE_SCAN'),
    ('FDS_QNOISE_SCAN','FREQ_DEPENDENT_SQZ'),
    ('FREQ_DEPENDENT_SQZ','PREP_SCAN'),
    ('PREP_SCAN','SCAN_ALIGNMENT'),
    ('SCAN_ALIGNMENT','ZERO_AS42_OFFSET'),
    ('SCAN_ALIGNMENT','SQZ_ALIGNED'),
    ('ZERO_AS42_OFFSET','SQZ_ALIGNED'),
    ('SQZ_ALIGNED','REVERT'),
    ('REVERT','FREQ_DEPENDENT_SQZ'),

    ('PREP_SCAN','SCAN_SQZANG'),
    ('SCAN_SQZANG','SQZANG_OPTIMIZED'),
    ('SQZANG_OPTIMIZED','REVERT'),
    ('REVERT','FREQ_DEPENDENT_SQZ'),

    ('WAIT_IFO_1','REALIGN_FC2'),
    ('REALIGN_FC2','LOCK_OPO'),
    

    ('RESET','DOWN'),
    ('DOWN', 'NO_SQUEEZING'),
    ('NO_SQUEEZING', 'RESET'),

    ('NEEDS_USER_INTERVENTION', 'RESET'),
    
    ('FREQ_DEPENDENT_SQZ','SWITCH_FDS_TO_FIS'),
    ('SWITCH_FDS_TO_FIS','MISALIGN_FC2_2ND'),
    
    ('FREQ_INDEPENDENT_SQZ', 'SWITCH_FIS_TO_FDS'),
    ('SWITCH_FIS_TO_FDS', 'LOCK_FC'),

    ('OPEN_BDIV_2ND','MISALIGN_FC2_2ND'),
    ('MISALIGN_FC2_2ND','LOCK_LO'),
    ('LOCK_LO','FREQ_INDEPENDENT_SQZ'),

]



'''



class ALIGNING_SQZ(GuardState):
    index = 140
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS',  'SQZ_ISS', 'SQZ_FC'])
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']
        _nodes = NodeManager(['SQZ_LO',])
        self.LO_node = _nodes['SQZ_LO']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.CLF_node.STALLED or self.LO_node.STALLED:
            return 'IDLE'
        
        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return

        if not self.LO_node.completed:
            notify('Waiting LO guardian')
            return
        

        if self.counter == 0:
            self.CLF_node.set_request('OMCTRANS_Q_MAXIMIZED')
            self.counter += 1

        elif self.counter == 1:
            self.LO_node.set_request('ADS_ENGAGED')
            self.counter += 1


        elif self.counter == 2:
            return True

        
#-------------------------------------------------------------------------------
class SQZ_ALIGNED(GuardState):
    index = 200
    request = True
    DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_CLF', 'SQZ_ISS', 'SQZ_FC'])
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        self.counter = 0
        self.timer['done'] = 0

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        return True

    
class DISABLE_SQZASC(GuardState):
    index = 250
    request = False
    DEPS = frozenset(['SQZ_SUS', 'SQZ_SHG', 'SQZ_OPO', 'SQZ_TTFSS', 'SQZ_ISS', 'SQZ_FC'])
    def main(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump
        
        self.counter = 0
        self.timer['done'] = 0

        self.CLF_node = nodes['SQZ_CLF']
        
        _nodes = NodeManager(['SQZ_LO',])
        self.LO_node = _nodes['SQZ_LO']

    def run(self):
        bdep_jump = are_dependencies_bad(self.DEPS)
        if bdep_jump: return bdep_jump

        if self.CLF_node.STALLED or self.LO_node.STALLED:
            return 'IDLE'
        
        if not self.CLF_node.completed:
            notify('Waiting CLF guardian')
            return
        
        if not self.LO_node.completed:
            notify('Waiting LO guardian')
            return

        if self.counter == 0:
            self.LO_node.set_request('LOCKED')
            self.counter += 1

        elif self.counter == 1:
            self.CLF_node.set_request('CLF_REFL_LOCKED')
            self.counter += 1

        elif self.counter == 2:
            return True
'''
